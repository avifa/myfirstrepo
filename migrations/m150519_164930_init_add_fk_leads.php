<?php

use yii\db\Schema;
use yii\db\Migration;

class m150519_164930_init_add_fk_leads extends Migration
{
    public function up()
    {
		$this->addColumn('lead','ownerId','integer');
		$this->addForeignKey('lead_user_owner','lead','ownerId','user','id');
    }

    public function down()
    {
		$this->dropForeignKey('lead_user_owner','lead');
		$this->dropColumn('lead','ownerId');
	
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
