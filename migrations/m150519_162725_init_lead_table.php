<?php

use yii\db\Schema;
use yii\db\Migration;

class m150519_162725_init_lead_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'lead',
			[
				'id'=>'pk',
				'name'=>'string',
				'birth_date'=>'date',
				'status'=>'int',
				'notes'=>'text',
				
			],
			'ENGINE=InnoDB'
		);
    }

    public function down()
    {
        $this->dropTable('lead');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
