<?php

namespace app\models;

use Yii;
use \yii\web\IdentityInterface;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
	public $role;
	public function getLeads()
	{
		return $this->hasMany(lead::className(),['OwnerId'=>'id']);
	}
	public static function getRoles()
	{
		return ['user'=>'user','manager'=>'manager','admin'=>'admin'];
	}
	public static function tableName()
	{
		return 'user';
	}

	public function rules()
	{
		return [
				[['username','password','auth_key'],'string','max'=>255],
				[['username','password'],'required'],
				[['username'],'unique']
			];
	}
	public function attributeLabels()
	{
		return [
			'id'=>'ID',
			'username'=>'Username',
			'password'=>'Password',
			'auth_key'=>'auth_key',
		];
	}
  
    public static function findIdentity($id)
    {
        return static::findOne($id);
	}
	
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('You can only login by username/password pair for now.');
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username'=> $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthkey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthkey($auth_key)
    {
        return $this->auth_key === $auth_key;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password,$this->password);
    }
	
	 public function  beforeSave($insert)
	 {
		$return = parent::beforeSave($insert);
		
		if($this->isAttributeChanged('password'))
		{
			$this->password=Yii::$app->security->generatePasswordHash($this->password);
		}
		if($this->isNewRecord)
		{
			$this->auth_key= Yii::$app->security->generateRandomString(32);
		}	
		return $return;
	 
	 }
	 
	/* public function afterSave($insert,$changedAttributes)
     {
        $return = parent::afterSave($insert, $changedAttributes);

        $auth = Yii::$app->authManager;
		$roleName = $this->role; 
		$authorRole = $auth->getRole($roleName);
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null)
		{
			$auth->assign($authorRole, $this->id);
		} 
		else 
		{
			$db = \Yii::$app->db;
			$db->createCommand()->delete('auth_assignment', ['user_id' => $this->id])->execute();
			$auth->assign($authorRole, $this->id);
		}

        return $return;
     }	
	*/ 
}
