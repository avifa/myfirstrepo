<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lead".
 *
 * @property integer $id
 * @property string $name
 * @property string $birth_date
 * @property integer $status
 * @property string $notes
 * @property integer $ownerId
 *
 * @property User $owner
 */
class Lead extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lead';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birth_date'], 'safe'],
            [['status', 'ownerId'], 'integer'],
            [['notes'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'birth_date' => 'Birth Date',
            'status' => 'Status',
            'notes' => 'Notes',
            'ownerId' => 'User responsible',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'ownerId']);
    }
}
